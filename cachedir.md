---
title: "`cachedir`&mdash;cache directory tag management"
author: The Obnam project
bindings:
  - lib/files.yaml
  - lib/runcmd.yaml
  - subplot.yaml
impls:
  rust:
  - subplot.rs
...

# Introduction

[Cache Directory Tagging Specification]: http://www.bford.info/cachedir/

The `cachedir` utility manages tags for cache directories. A cache
directory tag is specified in [Cache Directory Tagging
Specification][], and is a file called `CACHEDIR.TAG`, where the first
line consists of:

~~~
Signature: 8a477f597d28d172789f06886806bc55
~~~

Creating such files by hand is easy enough, but `cachedir` makes it a
little easier.

Many backup programs recognize cache directory tags and can exclude
such directories.

# Data files for testing

We will use a dummy tag file for testing: something that is named
`CACHEDIR.TAG`, but doesn't have the right contents.

~~~{#not-a-tag .file}
This is not a cache directory tag.
~~~


# Managing cache directories

_Requirement: The tool must find cache directories, and add and remove
tags as requested._

We should find no cache directories, if there aren't any.

~~~scenario
given an installed cachedir
when I run cachedir find .
then stdout is exactly ""
when I try to run cachedir is-cache foo
then command fails
when I try to run cachedir is-cache bar
then command fails
~~~

When we create a directory, and a `CACHEDIR.TAG` file with the wrong
contents, it's not a cache directory.

~~~scenario
given a directory foo
given file foo/CACHEDIR.TAG from not-a-tag
when I run cachedir find .
then stdout is exactly ""
when I try to run cachedir is-cache foo
then command fails
when I try to run cachedir is-cache bar
then command fails
~~~

When we create an actual tag, the directory is found.

~~~scenario
given a directory bar
when I run cachedir tag bar
when I run cachedir find .
then stdout contains "/bar"
when I try to run cachedir is-cache foo
then command fails
when I try to run cachedir is-cache bar
then command is successful
~~~

Finally, we should again find no cache directories if we remove that tag.

~~~scenario
when I run cachedir untag bar
when I run cachedir find .
then stdout is exactly ""
when I try to run cachedir is-cache foo
then command fails
when I try to run cachedir is-cache bar
then command fails
~~~
