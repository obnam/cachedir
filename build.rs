use std::path::Path;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    subplot_build::codegen(Path::new("cachedir.subplot"))
        .expect("failed to generate code with Subplot");
}
