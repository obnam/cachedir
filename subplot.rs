// Rust support for running sq-subplot.md scenarios.

use subplotlib::steplibrary::runcmd::Runcmd;

use std::path::Path;

#[step]
#[context(Runcmd)]
fn install_cachedir(context: &ScenarioContext) {
    // The CACHEDIR_DIR variable can be set to test an installed
    // cachedir rather than the one built from the source tree.
    if let Some(bindir) = std::env::var_os("CACHEDIR_DIR") {
        println!("Found CACHEDIR_DIR environment variable, using that");
        context.with_mut(
            |rc: &mut Runcmd| {
                rc.prepend_to_path(bindir);
                Ok(())
            },
            false,
        )?;
    } else {
        let target_exe = env!("CARGO_BIN_EXE_cachedir");
        let target_path = Path::new(target_exe);
        let target_path = target_path.parent().ok_or("No parent?")?;

        context.with_mut(
            |context: &mut Runcmd| {
                context.prepend_to_path(target_path);
                Ok(())
            },
            false,
        )?;
    }
}
